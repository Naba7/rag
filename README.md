# RAG



## Getting started

This project is based on retrieval augmented generation. The LLM in langchain generates response from the documents already present in the DB.

#### Langchain

The most basic unit of langchain is a llm. A basic architecture/wrapper for popular llms are present in langchain such as openai, anthropic, cohere, llamacpp, gpt4all, huggingface_hub, and many more. There are various other parameters to be set which are specific for each of the wrappers.

We will be using prompt templates instead of prompts for each of the varying queries during every other iteration.
We will be using chains to send in primitives like llms or chains of inputs so that gets executed once the all the links for the segments are available.

###### Indexing documents

LangChain primary focuses on constructing indexes with the goal of using them as a Retriever. Question answering over documents consists of four steps:

- Create an index

- Create a Retriever from that index

- Create a question answering chain

- Ask questions!

First of all, create an .env file in the rag/ directory and change the ``$`` on line ``OPENAI_API_KEY=$`` to the value of your own OPENAI API KEY.

## Test

```
cd rag
pip install -r requirements.txt
python final.py
```
You need to press ``q`` or ``e`` or ``quit`` or ``exit`` to end the chat session.

## Steps after running the final.py file

1. Type in your text/phrase/paragraph
2. Type in the type of response you want

## Outputs you receive

1. The text you typed in
2. The reply GPT sends in response to your text and desired type of response
3. The most similar response to your text will be scoured through the entire DB and presented

## OSC

ip = "127.0.0.1"

inPort = 5000

sendPort = 6000


## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/Naba7/rag.git
git branch -M main
git push -uf origin main
```

## 