from dotenv import load_dotenv
import os
import csv
import numpy as np
import pandas as pd
import argparse
from pythonosc import udp_client
from time import sleep
import time
load_dotenv()
OPENAI_API_KEY=os.getenv("OPENAI_API_KEY")

# Multiple docs but get the relevant ones only
import os
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import FAISS
from langchain.llms import OpenAI
from langchain.chains import RetrievalQA
from langchain.prompts import PromptTemplate


# path joining version for other paths
filename = "./chats.csv"
total_chats = len(pd.read_csv(filename))
counter = total_chats + 1

texts = []
# opening the CSV file
with open('chats.csv', mode ='r') as file:
   
    # reading the CSV file
    CSVFile = csv.reader(file)
    texts = [lines for lines in CSVFile]

texts = np.asarray(texts).ravel()

embeddings = OpenAIEmbeddings()
docsearch = FAISS.from_texts(texts, embeddings)

def create_prompt_query(query, reply_type):
    # Sentence matching prompt
    template = """Use the following pieces of {question} as a reply to the query at the end. If you don't know the answer, make up a {context} using similar words or context from the source file. Always reply from following pieces of context or source data file.
    {context}
    If the {question} is a {context}, then reply as the best suited {context}.
    Query: {question}
    Answer in any language that you know:"""

    PROMPT = PromptTemplate(
        input_variables=["question", "context"], template=template,
    )

    chain_type_kwargs = {"prompt": PROMPT}
    vectordbkwargs = {"search_distance": 0.1} # predictions be more random, then higher the temperature
    qa = RetrievalQA.from_chain_type(llm=OpenAI(), chain_type="stuff", retriever=docsearch.as_retriever(search_type='similarity'), chain_type_kwargs=chain_type_kwargs, return_source_documents=True, verbose=False) # keep verbose=True during dev stage
    result = qa({'query': query, 'vectordbkwargs': vectordbkwargs})
    return result


if __name__=="__main__":
    ip = "127.0.0.1"
    sendPort = 6000
    inPort = 5000

    while True:

        query = input("Enter your text here ... ")

        if query == "quit" or query == "q" or query == "exit" or query == "e":
            break

        reply_type = input("Enter what kind of reply you want ... ")

        result = create_prompt_query(query, reply_type)

        print("You said -> " + query)
        print("GPT replies -> " + result["result"])
        print("DB response -> " + result["source_documents"][0].page_content)

        counter = counter + 1
        content = str(counter) + " - " + query + "\n"
        with open('chats.csv', 'a') as file:
            wr = csv.writer(file)
            wr.writerow([content])


        client = udp_client.SimpleUDPClient(ip, inPort)
        client.send_message("/gptReply", result["result"])
        client.send_message("/dbReply", result["source_documents"][0].page_content)
        time.sleep(1)
  
